import React from 'react';
import { AutoComplete, Skeleton, Typography } from 'antd';
import 'antd/dist/antd.css';
import './index.css';

const { Title, Text } = Typography;

class App extends React.Component {
  state = {
    value: '',
    dataSource: [],
    isSelected: false,
    isWeatherLoading: false,
    weatherInfo: {},
  };

  onSelect = (value) => {
    console.log('onSelect', value);
    this.setState({isSelected: true});
    this.fetchWeather(value);
  }

  onChange = value => {
    console.log("onChange", value);
    if (!Number.isInteger(value)) {
      this.fetchLocation(value);
      this.setState({ value });
    }
  };

  handleChange = e => {
    console.log(e)
    this.setState({ location: e.target.value });
  };

  fetchWeather = locid => {
    this.setState({isWeatherLoading: true});

    // Temporary solution to cors
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    fetch(proxyurl + "https://www.metaweather.com/api/location/" + locid, {mode: 'cors'})
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          let weatherUsed = result.consolidated_weather[0];
          let weather = weatherUsed.weather_state_name;
          let weatherLogo = weatherUsed.weather_state_abbr;
          let temp =  Number((weatherUsed.the_temp).toFixed());
          let windSpeed = Number((weatherUsed.wind_speed).toFixed(2));
          let windDirection = Number((weatherUsed.wind_direction).toFixed());
          let airPressure = weatherUsed.air_pressure;
          let humidity = weatherUsed.humidity;
          let visibility = Number((weatherUsed.visibility).toFixed(2));
          let country = result.parent.title;
          let city = result.title;
          this.setState({
            weatherInfo: {
              weather: weather,
              weatherLogo: "https://www.metaweather.com/static/img/weather/" + weatherLogo + ".svg",
              temp: temp,
              windSpeed: windSpeed,
              windDirection: windDirection,
              airPressure: airPressure,
              humidity: humidity,
              visibility: visibility,
              country: country,
              city: city
            },
            isWeatherLoading: false
          });
        },
      )
  }

  fetchLocation = location => {
    console.log('fetchWeather', location);
    if (!location) {
      this.setState({
        dataSource: [],
      });
      return;
    }
    // Temporary solution to cors
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    fetch(proxyurl + "https://www.metaweather.com/api/location/search/?query=" + location, {mode: 'cors'})
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          let parsed = result.map(item => {
            return {
              value: item.woeid,
              text: item.title
            }
          })
          this.setState({
            dataSource: parsed,
          });
        },
      )
  }

  renderWeather() {
    const { isWeatherLoading, isSelected, weatherInfo } = this.state;
    if (!isSelected) {
      return null;
    }
    if (isWeatherLoading) {
      return(<Skeleton active />)
    }
    console.log(weatherInfo.weatherLogo)
    return (
      <div style={{ margin: "10px auto" }}>
        <Title level={2}>{weatherInfo.city}, {weatherInfo.country}</Title>
        <div style={{ position:"relative", margin: '10px auto' }}>
          <img src={weatherInfo.weatherLogo} style={{ align:"center", marginRight:10, width:40, height:40,
          position:"absolute" }} alt="hr"/>
          <Text style={{ fontSize:32, position:"relative", marginLeft:50}}>{weatherInfo.weather}, {weatherInfo.temp}°C</Text>
        </div>
        <Text>Wind: {weatherInfo.windSpeed} mph at {weatherInfo.windDirection}°</Text>
        <br/>
        <Text>Humidity: {weatherInfo.humidity}%</Text>
        <br/>
        <Text>Air Pressure: {weatherInfo.airPressure} mbar</Text>
        <br/>
        <Text>Visibility: {weatherInfo.visibility} miles</Text>
      </div>
    );
  }

  render() {
    const { dataSource, value } = this.state;
    return (
      <div style={{ width: 768, margin: '50px auto' }}>
        <Title>Weather</Title>
        <p>This application is powered by <a href="https://www.metaweather.com/api/">Metaweather</a>.</p>
        <AutoComplete
          value={value}
          dataSource={dataSource}
          style={{ width: '100%' }}
          onSelect={this.onSelect}
          onChange={this.onChange}
          placeholder="Enter location"
        />
        {this.renderWeather()}
      </div>
    );
  }
}

export default App;